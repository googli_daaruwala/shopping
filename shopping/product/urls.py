from  django.urls import path
from .views import product_list_view, product_detail_view, product_create_view, product_delete_view

urlpatterns = [
    path('product_list/', product_list_view, name='product-list'),
    path('product_detail/<int:id>/', product_detail_view, name='product-detail'),
    path('product_create/', product_create_view, name='product-create'),
    path('product_delete/<int:id>', product_delete_view, name='product-delete'),
]