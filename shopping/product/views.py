from django.shortcuts import render, get_object_or_404, redirect
from .models import product
from .forms import ProductForm
# Create your views here.

def product_list_view(request):
    obj = product.objects.all()
    context = {
        'object':obj
    }
    return render(request,'product/product_list.html', context)

def product_detail_view(request, id):
    obj = get_object_or_404(product, id=id)
    context = {
        'object':obj
    }
    return render(request,'product/product_detail.html', context)

def product_create_view(request):
    form = ProductForm(request.POST)
    if form.is_valid():
        form.save()
    context = {
        'form':form
    }
    return render(request,'product/product_create.html', context)

def product_delete_view(request, id):
    obj = get_object_or_404(product, id=id)
    if request.method == 'POST':
        obj.delete()
        return redirect('product-list')
    context = {
        'object':obj
    }
    return render(request,'product/product_delete.html', context)