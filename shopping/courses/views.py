from django.shortcuts import render, get_object_or_404, redirect
from django.views import View
from .models import CourseModel
from .forms import CourseModelForm
# Create your views here.


class CourseListView(View):
    queryset = CourseModel.objects.all()
    def get(self, request, *args, **kwargs):
        context = {
            "object_list" : self.queryset
        }
        return render(request, 'courses/course_list.html', context) #here, self.template_name can also be used with the  template_name outsede the  function but but but!
                                                    #this  can furthur be overridden by using template in urls.py  

class CourseDetailView(View):
    template_name = 'courses/course_detail.html'
    def get(self, request, id=None, *args, **kwargs):
        context = {}
        if id is not None:
            obj = get_object_or_404(CourseModel, id=id)
            context['object'] = obj
            return render(request, self.template_name, context)

class CourseCreateView(View):
    template_name = 'courses/course_create.html'
    def get(self, request, *args, **kwargs):
        form = CourseModelForm()
        context = {
            "form": form
        }
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        form = CourseModelForm(request.POST)
        context = {
            "form":form
        }
        if form.is_valid():
            form.save()
        return render(request, self.template_name, context)

class CourseUpdateView(View):
    template_name = 'courses/course_update.html'

    def get_object(self):
        id = self.kwargs.get("id")
        obj = None
        if id is not None:
            obj = get_object_or_404(CourseModel, id=id)
        return obj

    def get(self, request, id=None, *args, **kwargs):
        context = {}
        obj = self.get_object()
        if obj is not None:
            form = CourseModelForm(instance=obj)
            context['object'] = obj
            context['form'] = form
        return render(request, self.template_name, context)

    def post(self, request, id=None, *args, **kwargs):
        context = {}
        obj = self.get_object()
        if obj is not None:
            form = CourseModelForm(request.POST, instance=obj)
            if form.is_valid():
                form.save()
            context['object'] = obj
            context['form'] = form
        return render(request, self.template_name, context)

class CourseDeleteView(View):
    template_name = 'courses/course_delete.html'

    def get_object(self):
        id = self.kwargs.get("id")
        obj = None
        if id is not None:
            obj = get_object_or_404(CourseModel, id=id)
        return obj

    def get(self, request, id=None, *args, **kwargs):
        context = {}
        obj = self.get_object()
        if obj is not None:
            form = CourseModelForm(instance=obj)
            context['object'] = obj
            context['form'] = form
        return render(request, self.template_name, context)

    def post(self, request, id=None, *args, **kwargs):
        context = {}
        obj = self.get_object()
        if obj is not None:
            form = CourseModelForm(request.POST, instance=obj)
            obj.delete()
            context['object'] = obj
            context['form'] = form
            return redirect('course-list')
        return render(request, self.template_name, context)