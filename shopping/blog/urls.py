from django.urls import path
from .views  import ArticleListView, ArticleDetailView, ArticleCreateView, ArticleUpdateView, ArticleDeleteView

urlpatterns = [
    path('article_list/', ArticleListView.as_view(), name='article-list'),
    path('article_detail/<int:id>/', ArticleDetailView.as_view(), name='article-detail'),
    path('article_create/', ArticleCreateView.as_view(), name='article-create'),
    path('article_update/<int:id>/', ArticleUpdateView.as_view(), name='article-update'),
    path('article_delete/<int:id>/',ArticleDeleteView.as_view(), name='article-delete'),
]