from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.views.generic import (
    ListView, 
    DetailView, 
    CreateView,
    UpdateView,
    DeleteView,
)
from .models import Article
from .forms import ArticleForm
# Create your views here.

class ArticleListView(ListView):
    queryset = Article.objects.all() #defaul template <appname>/<modlename>_<viewtype> ....it can be easily overridden by using template_name="<route to template>"

class ArticleDetailView(DetailView):
    queryset = Article.objects.all() #it will work perfect without this command ,  yet used to narrow the search

    def get_object(self):
        id_ = self.kwargs.get("id")
        return get_object_or_404(Article, id=id_)

class ArticleCreateView(CreateView):
    form_class = ArticleForm
    queryset = Article.objects.all()
    template_name = 'blog/article_create.html'
    #here a success url can be given to over ride the get absolute url method in models.py

    def form_valid(self, form):
        return super().form_valid(form)

class ArticleUpdateView(UpdateView):
    form_class = ArticleForm
    queryset = Article.objects.all()
    template_name = 'blog/article_update.html'

    def get_object(self):
        id_ = self.kwargs.get("id")
        return get_object_or_404(Article, id=id_)

    def form_valid(self, form):
        return super().form_valid(form)

class ArticleDeleteView(DeleteView):
    queryset = Article.objects.all()
    template_name = 'blog/article_delete.html'

    def get_object(self):
        id_ = self.kwargs.get("id")
        return get_object_or_404(Article, id=id_)

    def get_success_url(self): #success  url can also be used  but this  is also an  alternate way for more precise routing ....get absolute url in models
                                #as the object does not exist anymore
        return reverse('article-list')